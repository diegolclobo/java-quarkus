package br.com.lyncas;

import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

import br.com.lyncas.Service.BookService;
import br.com.lyncas.model.Book1;

@Path("/volumes")
public class BooksResource {

	@Inject
    @RestClient
    BookService service;
	
    @GET
    @RolesAllowed("admin")
    @Produces(MediaType.APPLICATION_JSON)
    public Set<Book1> volumes(@QueryParam("q") String q) {
        return service.volumes(q);
    }
}