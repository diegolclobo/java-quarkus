package br.com.lyncas.Service;

import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import org.jboss.resteasy.annotations.jaxrs.QueryParam;

import br.com.lyncas.model.Book1;


@RegisterRestClient
public interface BookService {

	@GET
    @Produces("application/json")
	Set<Book1> volumes(@QueryParam("q") String q);
}
